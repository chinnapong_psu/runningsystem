import React from "react";
import { useNavigate } from "react-router-dom";
import { Fragment } from "react";
import {
  Avatar,
  Button,
  TextField,
  Link,
  Grid,
  Box,
  Typography,
  Container,
} from "@mui/material";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import axios from "axios";
import { useEffect } from "react";
import AppBarComp from "../Components/appBarComp";


const LogInPage = () => {
  let navigate = useNavigate();

  useEffect(()=>{
    if(sessionStorage["token"]){
        navigate(`/home`); 
    }
  },[])

  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);

    let payload = {
      username: data.get("username"),
      password: data.get("password"),
    };

    let api_root = process.env.REACT_APP_API_ROOT
    let login_path = api_root+"/login"
    axios.post(login_path, null, { params: payload } )
        .then(  res => { 
                  let login_result = res.data
                  if (login_result.status == "200"){
                    alert("Login Success")
                    sessionStorage.setItem('token', res.data.token)
                    navigate(`/home`); 
                  }else {
                    alert(res.data.message)
                  }    
              }
        );
    
  };

  return (
    <Fragment>
      <AppBarComp  />
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
          <TextField
            margin="normal"
            required
            fullWidth
            id="Username"
            label="Username"
            name="username"
            autoComplete="Username"
            autoFocus
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="Password"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item>
              <Link
                href="#"
                variant="body2"
                onClick={() => navigate(`/register`)}
              >
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Fragment>
  );
};

export default LogInPage;
