import React, { Fragment, useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Grid,
  Box,
  Typography,
  Container,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Stack,
} from "@mui/material";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";

import { runningEventData } from "../Data/runningEventData";
import { registerHistoryData } from "../Data/registerHistoryData";
import { runner } from "../Data/runner";

import AppBarComp from "../Components/appBarComp";
import axios from "axios";


const columns = [
  { id: "Runner Name", label: "Runner Name", minWidth: 100 },
  {
    id: "Runner Surname",
    label: "Runner Surname",
    minWidth: 170,
  },
  {
    id: "Distance",
    label: "Distance",
    minWidth: 170,
    format: (value) => value.toFixed(2),
  },
  {
    id: "Registration Time",
    label: "Registration Time",
    minWidth: 170,
  },
];

const RegisterEventPage = () => {
  let { id } = useParams();
  let navigate = useNavigate();
  const [eventName, setEventName] = useState("");
  const [eventLocation, setEventLocation] = useState("");
  const [registerData, setRegisterData] = useState("");
  const [runnerData, setRunnerData] = useState("");
  const [selectDistance, setSelectDistance] = useState(10.5);


  const loadData = () => {
    let payload = {
      event_id: id,
    };
    console.log("Payload : ", payload)
    let api_root = process.env.REACT_APP_API_ROOT
    let list_path = api_root+"/list_reg_event"
    axios.get(list_path, { params: payload } ).then( res=> {
      setEventName(res.data[0].EventName)
      setEventLocation(res.data[0].EventLocation)
      setRegisterData(res.data);
      console.log(JSON.stringify(res.data))
    })

  }
  useEffect(() => {
    //fetch data
    // [{...}, {...}, ...]
    if(! ("token" in sessionStorage) ){
      navigate(`/`); 
    }

    loadData();

  }, []);

  console.log(runnerData);

  const registerEvent = () => {
    //coding register logic here
    alert("yesss");

    let payload = {
      event_id: id,
      distance: selectDistance,
    };
    let api_root = process.env.REACT_APP_API_ROOT
    let register_path = api_root+"/register_event"
    axios.post(register_path, null, { params: payload,
                                     headers: {'Authorization': 'Bearer '+sessionStorage['token']}   
                                  })
        .then(  res => { 
         
            alert("Register success")
            loadData()
        });
  };

  return (
    <Fragment>
       <AppBarComp  />
      <Box
        sx={{
          bgcolor: "background.paper",
          pt: 8,
          pb: 6,
        }}
      >
        <Container maxWidth="sm">
          <Typography
            component="h1"
            variant="h3"
            align="center"
            color="text.primary"
            gutterBottom
          >
            {eventName}
          </Typography>
          <Typography
            variant="h5"
            align="center"
            color="text.secondary"
            paragraph
          >
            Location: {eventLocation}
          </Typography>
          <Stack
            sx={{ pt: 4 }}
            direction="row"
            spacing={2}
            justifyContent="center"
          >
            <FormControl>
              <InputLabel>ระยะที่ลง</InputLabel>
              <Select
                value={selectDistance}
                label="Distance"
                onChange={(e)=> { setSelectDistance(e.target.value) }}
              >
                <MenuItem value={10.5}>Mini</MenuItem>
                <MenuItem value={21}>Half</MenuItem>
                <MenuItem value={42.5}>Marathon</MenuItem>
              </Select>
          </FormControl>

            <Button variant="contained" onClick={() => registerEvent()}>
              Register 
            </Button>
            {/* <Button variant="outlined">Applicants</Button> */}
          </Stack>
        </Container>
        <Paper sx={{ width: "100%", overflow: "hidden", pt: 8 }}>
          <TableContainer sx={{ maxHeight: 440 }}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  {columns.map((column) => (
                    <TableCell
                      key={column.id}
                      align={column.align}
                      style={{ minWidth: column.minWidth }}
                    >
                      {column.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
              
                         
                          
                          {registerData && registerData.map((value) => {
                            return (
                              <TableRow
                                hover
                                role="checkbox"
                                tabIndex={-1}
                              >
                                <TableCell>{value.RunnerName}</TableCell>
                                <TableCell>{value.RunnerSurname}</TableCell>
                                <TableCell>{value.Distance}</TableCell>
                                <TableCell>{value.RegistrationTime}</TableCell>
                               </TableRow>
                            );
                          })}
            
                       
                  
              
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </Box>
    </Fragment>
  );
};

export default RegisterEventPage;
