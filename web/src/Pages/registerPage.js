import React from "react";
import { useNavigate } from "react-router-dom";
import {
  Avatar,
  Button,
  TextField,
  Link,
  Grid,
  Box,
  Typography,
  Container,
} from "@mui/material";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import axios from "axios";

import AppBarComp from "../Components/appBarComp";

const RegisterPage = () => {
  let navigate = useNavigate();

  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);

    let payload = {
      runner_name : data.get("firstname"),
      runner_surname: data.get("surname"),
      runner_username: data.get("username"),
      runner_password: data.get("password"),
    };
    console.log(payload)

    let api_root = process.env.REACT_APP_API_ROOT
    let signup_path = api_root+"/register_runner"
    axios.post(signup_path, null, { params: payload } )
        .then(  res => { 
                  let signup_result = res.data
                  if (signup_result.status == "200"){
                    alert("Signup Success")
                    navigate(`/`); 
                  }else {
                    alert("Error signing up, verify your form data")
                  }    
              }
        );
  };

  return (
    <Container component="main" maxWidth="xs">
      <AppBarComp  />
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
          <TextField
            margin="normal"
            required
            fullWidth
            id="firstname"
            label="First Name"
            name="firstname"
            autoComplete="firstname"
            autoFocus
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="surname"
            label="Surname"
            type="surname"
            id="surname"
            autoComplete="surname"
          />

          <TextField
            margin="normal"
            required
            fullWidth
            name="username"
            label="Username"
            type="Username"
            id="username"
            autoComplete="username"
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="password"
          />

          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Sign Up
          </Button>
          <Grid container>
            <Grid item>
              <Link
                href="#"
                variant="body2"
                onClick={() => navigate(`/`)}
              >
                {"Already have an account? Back to Sign in"}
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Container>
  );
};

export default RegisterPage;
