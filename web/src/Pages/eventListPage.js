import { React, Fragment, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Grid,
  Box,
  Typography,
  Container,
} from "@mui/material";

//import { runningEventData } from "../Data/runningEventData";
import AppBarComp from "../Components/appBarComp";
import axios from 'axios';

const EventListPage = () => {
  let navigate = useNavigate();
  const [runningEventData, setRunningEventData] = useState([]); 

  useEffect( () => {
    if(! ("token" in sessionStorage) ){
      navigate(`/`); 
    }

    let api_root = process.env.REACT_APP_API_ROOT
    let eventlist_path = api_root+"/list_event"
    axios.get(eventlist_path).then( res => {
        setRunningEventData(res.data)
        console.log(res.data)
    })
  }, [])

  return (
    <Fragment>
      <AppBarComp auth={true} />
      <Box
        sx={{
          bgcolor: "background.paper",
          pt: 8,
          pb: 6,
        }}
      >
        <Container maxWidth="sm">
          <Typography
            component="p"
            variant="h3"
            align="center"
            color="text.primary"
            gutterBottom
          >
            รายการงานวิ่งที่เปิดรับสมัคร
          </Typography>
          <Typography
            variant="h5"
            align="center"
            color="text.secondary"
            paragraph
          >
            สมัครงานวิ่งต่อไปนี้กันตามสบายนะฮะ
          </Typography>
        </Container>
      </Box>
      <Container sx={{ py: 8 }} maxWidth="md">
        {/* End hero unit */}
        <Grid container spacing={4}>
          {runningEventData.map((row) => (
            <Grid item key={row.EventID} xs={12} sm={6} md={4}>
              <Card
                sx={{
                  height: "100%",
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                <CardContent sx={{ flexGrow: 1 }}>
                  <Typography gutterBottom variant="h5" component="h2">
                    {row.EventName}
                  </Typography>
                  <Typography>{row.EventLocation}</Typography>
                </CardContent>
                <CardActions>
                  <Button
                    size="small"
                    onClick={() => navigate(`/registerEvent/${row.EventID}`)}
                  >
                    Details
                  </Button>
                </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>
    </Fragment>
  );
};

export default EventListPage;
