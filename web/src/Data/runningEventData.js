export const runningEventData = [
    {
        eventId:"1",
        eventName:"TRANG Marathon 2020",
        eventLocation:"Trang",
    },
    {
        eventId:"2",
        eventName:"Run to Give Phuket",
        eventLocation:"Phuket",
    },
    {
        eventId:"3",
        eventName:"Phuket Night Run",
        eventLocation:"Phuket",
    },
    {
        eventId:"4",
        eventName:"Songkhla International Marathon",
        eventLocation:"Songkhla",
    },
    {
        eventId:"5",
        eventName:"Khao Phlai Dam BigMountain Run",
        eventLocation:"Nakhonsrithammarat",
    },
    {
        eventId:"6",
        eventName:"Khunthale Lake Half Marathon",
        eventLocation:"Suratthani",
    }
]