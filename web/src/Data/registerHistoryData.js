export const registerHistoryData = [
  {
    registrationId: "1",
    runnerId: "1",
    eventId: "1",
    distance: 5,
    registrationTime: "2022-05-5",
  },
  {
    registrationId: "2",
    runnerId: "1",
    eventId: "2",
    distance: 14.5,
    registrationTime: "2022-05-6",
  },
  {
    registrationId: "3",
    runnerId: "2",
    eventId: "1",
    distance: 21,
    registrationTime: "2022-05-10",
  },
  {
    registrationId: "4",
    runnerId: "3",
    eventId: "4",
    distance: 42,
    registrationTime: "2022-05-11",
  },
];
