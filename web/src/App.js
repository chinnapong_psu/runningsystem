import React, { Fragment } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import LoginPage from "./Pages/loginPage";
import RegisterPage from "./Pages/registerPage";
import EventListPage from "./Pages/eventListPage";
import RegisterEventPage from "./Pages/registerEventPage";

import AppBarComp from "./Components/appBarComp";

const App = () => {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<LoginPage />} />
          <Route path="register" element={<RegisterPage />} />
          <Route path="home" element={<EventListPage />} />
          <Route path="registerEvent/:id" element={<RegisterEventPage />} />
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default App;
