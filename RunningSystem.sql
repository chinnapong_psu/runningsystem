-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: mariadb
-- Generation Time: Mar 18, 2022 at 12:07 AM
-- Server version: 10.6.5-MariaDB-1:10.6.5+maria~focal
-- PHP Version: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `RunningSystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `Registration`
--

CREATE TABLE `Registration` (
  `RegistrationID` int(11) NOT NULL,
  `RunnerID` int(11) NOT NULL,
  `EventID` int(11) NOT NULL,
  `Distance` float NOT NULL,
  `RegistrationTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Registration`
--

INSERT INTO `Registration` (`RegistrationID`, `RunnerID`, `EventID`, `Distance`, `RegistrationTime`) VALUES
(2, 1, 1, 42.5, '2022-03-02 02:30:46'),
(4, 2, 1, 42.5, '2022-03-02 02:32:51'),
(5, 2, 4, 42.5, '2022-03-02 02:32:57'),
(6, 3, 1, 42.5, '2022-03-02 02:33:03'),
(7, 3, 4, 42.5, '2022-03-02 02:33:07'),
(8, 8, 1, 42.5, '2022-03-07 09:37:57'),
(10, 8, 3, 42.5, '2022-03-07 09:38:38'),
(11, 7, 3, 42.5, '2022-03-07 09:41:09'),
(12, 7, 3, 10.5, '2022-03-17 15:31:32'),
(13, 7, 3, 21, '2022-03-17 15:34:34'),
(14, 24, 4, 10.5, '2022-03-17 15:37:02');

-- --------------------------------------------------------

--
-- Table structure for table `Runner`
--

CREATE TABLE `Runner` (
  `RunnerID` int(11) NOT NULL,
  `RunnerName` varchar(50) NOT NULL,
  `RunnerSurname` varchar(50) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(200) NOT NULL,
  `IsAdmin` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Runner`
--

INSERT INTO `Runner` (`RunnerID`, `RunnerName`, `RunnerSurname`, `Username`, `Password`, `IsAdmin`) VALUES
(1, 'Chinna', 'Etche', 'chinna', '12345678', 0),
(2, 'Damkerng', 'Boonchuay', '', '', 0),
(3, 'Damkerng2', 'BoonChuay2', '', '', 0),
(4, 'Dandan', 'Trunk', '', '', 0),
(5, 'Chinna', 'Etche', '', '', 0),
(7, 'Chinna', 'Etxe2', 'Chinnapong.a', '$2b$10$9w53ALvxQVTMdfN6DMz.5Opgx/jjScehFGhk2YWbnhsFHJGo48wkC', 1),
(8, 'Chinna', 'Etxe2', 'Chinnapong.a2', '$2b$10$Hag8TDj54O3MIgcz1Wc4x.kgBmY96.n47V0uDbIEmm3EALolpdT1.', 1),
(21, 'Chinna1', 'Chin2', 'angsu1', '$2b$10$4clg3EITi8g6pdg46J8H1uC/yD17cAXuZpuRpvUmaHsSdAsCgGFY6', 0),
(22, 'Chinnapong', 'Angsuchotmetee2', 'chinnapong.a3', '$2b$10$pOwZJXAzZy9Df4fIx2Puy.zxl.RdLu6/wDmbl7cuKCwVdJllBi2yi', 0),
(23, 'Damdam', 'EtcheNe', 'chinnax', '$2b$10$L4U9fa57bPP6SHfT6X2OJeJnUxYwhEYhrK.xye6Tkb60VxCn9pXpu', 0),
(24, 'DamChin', 'DamBork', 'chinna2', '$2b$10$slQEF4OH3wg6O4f29i4yNOBq2TbUHkKHOn1JiFFhF8HuPDH7eLPeS', 0);

-- --------------------------------------------------------

--
-- Table structure for table `RunningEvent`
--

CREATE TABLE `RunningEvent` (
  `EventID` int(11) NOT NULL,
  `EventName` varchar(200) NOT NULL,
  `EventLocation` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `RunningEvent`
--

INSERT INTO `RunningEvent` (`EventID`, `EventName`, `EventLocation`) VALUES
(1, 'วิ่งวิดยา', 'ตึกฟักทอง'),
(3, 'วิ่งกับชินนา Gaiden', 'ริมอ่างอีกแล้วโว้ย'),
(4, 'วิ่งกับชินนาภาค2', 'ริมอ่างอีกที');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Registration`
--
ALTER TABLE `Registration`
  ADD PRIMARY KEY (`RegistrationID`),
  ADD KEY `RunnerID` (`RunnerID`),
  ADD KEY `EventID` (`EventID`);

--
-- Indexes for table `Runner`
--
ALTER TABLE `Runner`
  ADD PRIMARY KEY (`RunnerID`);

--
-- Indexes for table `RunningEvent`
--
ALTER TABLE `RunningEvent`
  ADD PRIMARY KEY (`EventID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Registration`
--
ALTER TABLE `Registration`
  MODIFY `RegistrationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `Runner`
--
ALTER TABLE `Runner`
  MODIFY `RunnerID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `RunningEvent`
--
ALTER TABLE `RunningEvent`
  MODIFY `EventID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Registration`
--
ALTER TABLE `Registration`
  ADD CONSTRAINT `EventID` FOREIGN KEY (`EventID`) REFERENCES `RunningEvent` (`EventID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `RunnerID` FOREIGN KEY (`RunnerID`) REFERENCES `Runner` (`RunnerID`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
